<?php
  include('resources/header.php');
?>
  <!--Main layout-->
  <main class=" mx-lg-5">
    <div class="container-fluid pt-sm-5 pt-lg-4">
        
        <div class="card mb-4">
  <div class="card-body">
    <p class="h4">Meu Perfil</p>
  </div>
</div>
        <!-- Card -->
        <div class="card promoting-card">

        <!-- Card content -->
        <div class="card-body d-flex flex-row">

        <!-- Avatar -->
        <img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-8.jpg" class="rounded-circle mr-3" height="50px" width="50px" alt="avatar">

        <!-- Content -->
        <div>

            <!-- Title -->
            <h4 class="card-title font-weight-bold mb-2">Nome do usuário</h4>
            <!-- Subtitle -->
            <p class="card-text"><i class="fas fa-medal pr-2"></i>Saldo: 1.500 sol</p>

        </div>

        </div>

        <!-- Card content -->
        <div class="card-body">
            <div class="container">
        <form>
            <div class="row">
                <div class="col-lg-6">
                     <div class="md-form input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text md-addon">Nome</span>
                          </div>
                          <input type="text" id="nome" name="nome" aria-label="First name" disabled="" class="form-control" placeholder="Carlos Antonio">
                        </div>
                    </div>

                     <div class="col-lg-6">
                     <div class="md-form input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text md-addon">Telefone</span>
                          </div>
                          <input type="text" id="telefone" name="telefone" aria-label="First name" disabled="" class="form-control" placeholder="77 777777">
                        </div>
                    </div>
                </div>

                <div class="row">
                <div class="col-lg-4">
                     <div class="md-form input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text md-addon">Cidade</span>
                          </div>
                          <input type="text" id="cidade" name="cidade" aria-label="First name" disabled="" class="form-control" placeholder="Jequié">
                        </div>
                    </div>

                     <div class="col-lg-2">
                     <div class="md-form input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text md-addon">Estado</span>
                          </div>
                          <input type="text" id="estado" name="estado" aria-label="First name" disabled="" class="form-control" placeholder="BA">
                        </div>
                    </div>

                     <div class="col-lg-6">
                     <div class="md-form input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text md-addon">Logadouro</span>
                          </div>
                          <input type="text" id="logadouro" name="logadouro" aria-label="First name" disabled="" class="form-control" placeholder="Rua X, Bairro Y">
                        </div>
                    </div>
                </div>
            </form>
                <div class="row">
                    <button class="btn btn-primary" id="btnEditar" name="btnEditar" onclick="liberarEditarPerfil()"><i class="fas fa-magic mr-1"></i> Editar perfil</button>
                    <button class="btn btn-primary" id="btnSalvar" name="btnSalvar" disabled=""><i class="fas fa-save mr-1"></i> Salvar alterações</button>
                </div>
    

         <div class="row">
            <div class="col-12"><h4 class="text-center mt-4"><b>Atividades Recentes</b></h4></div>
        </div>
        <div class="row">
            <table id="dtMaterialDesignExample" class="table table-striped" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm">Nome da ação
      </th>
      <th class="th-sm">Instituição
      </th>
      <th class="th-sm">Data de execução
      </th>
      <th class="th-sm">Pontos
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Tiger Nixon</td>
      <td>System Architect</td>
      <td>Edinburgh</td>
      <td>61</td>
    </tr>
    <tr>
      <td>Garrett Winters</td>
      <td>Accountant</td>
      <td>Tokyo</td>
      <td>63</td>
    </tr>
    <tr>
      <td>Ashton Cox</td>
      <td>Junior Technical Author</td>
      <td>San Francisco</td>
      <td>66</td>
    </tr>
    <tr>
      <td>Cedric Kelly</td>
      <td>Senior Javascript Developer</td>
      <td>Edinburgh</td>
      <td>22</td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <th>Name
      </th>
      <th>Position
      </th>
      <th>Office
      </th>
      <th>Age
      </th>
    </tr>
  </tfoot>
</table>
        </div>


        </div>
        </div>

        </div>
        <!-- Card -->
        
    </div>

  </main>
  <!--Main layout-->
<?php
  include('resources/footer.php');
?>

</body>

</html>