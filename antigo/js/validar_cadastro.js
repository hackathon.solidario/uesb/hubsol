var elTermo = document.getElementById('termos-uso');
var btnCadastrar = document.getElementById('btn-cadastro');

window.onload = ()=> {
    btnCadastrar.classList.add("disabled");

    elTermo.addEventListener("click", () => {
        if(elTermo.checked){
            //Habilitar botão
            btnCadastrar.classList.remove("disabled");
        }else{
            //disabilita botão de cadastro
            btnCadastrar.classList.add("disabled");
        }
    }, false); 

};