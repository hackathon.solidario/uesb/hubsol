<?php
  include('resources/header.php');
?>
  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
            
    <div class="card mb-4">
        <div class="card-body">
            <p class="h4">Cadastrar Produto</p>
        </div>
    </div>

    <div class="card promoting-card mb-0 pb-0">
        <!-- Card content -->
        <div class="card-body">
    <form class="text-center" id="formProduto" method="POST">
    <img src="img/placeholder.png" id="previa" class="img-fluid mb-4" width="200rem" >
        
    <div class="row mb-4 mt-0 pt-0">
        <!-- centralizar -->
        <div class="col-4"></div>
        <div class="col-4 md-form">

         <div class="file-field">
    <div class="btn btn-primary btn-sm float-left">
      <span>Escolher arquivo</span>
      <input id="imagem" name="imagem" type="file">
    </div>
  </div>

        </div>
        </div>
        <!-- centralizar -->
        <div class="col-4"></div>
</div>

<div class="form-row">
    <div class="container">
    <div class="md-form input-group">
        <div class="input-group-prepend">
            <span class="input-group-text md-addon" id="inputGroupMaterial-sizing-default">Título da Ação</span>
        </div>
        <input type="text" id="titulo" name="titulo" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroupMaterial-sizing-default">
    </div>

    <div class="md-form input-group">
        <div class="input-group-prepend">
            <span class="input-group-text md-addon" id="inputGroupMaterial-sizing-default">SOl/Pontuação</span>
        </div>
        <input type="text" id="pontuacao" name="pontuacao" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroupMaterial-sizing-default">
    </div>

</div>
</div>
<div class="form-row">
    <div class="container">
       <div class="md-form input-group">
        <div class="input-group-prepend">
            <span class="input-group-text md-addon" id="inputGroupMaterial-sizing-default">Descrição do Produto</span>
        </div>
        <input type="text" id="descricao" name="descricao" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroupMaterial-sizing-default">
    </div>
    </div>
    </div>
</div>

<!-- Sign up button -->
<button class="btn btn-success btn-block col-12 col-md-2" type="submit">Salvar</button>
<hr>
</form>
    </div>

  </main>
  <!--Main layout-->
<?php
  include('resources/footer.php');
?>
<script type="text/javascript" src="font/toastr/toastr.min.js"></script>
 <script>
    toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "200",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
  </script>
<script>

//função para salvar formulário 
$(document).ready(function () {
    $('#formProduto').submit(function() {
        var dados = $('#formProduto').serialize();
        $.ajax({
                type: 'POST',
                dataType: 'json',
                url: 'include/produto.php',
                async: true,
                data: dados,
                success: function(data) {
                    if(data=="OK"){
                        toastr.success('Sucesso - operação realizada!');
                    }else if(data=="ERRO"){
                        toastr.error('Erro - Não foi possível realizar operação.!');
                    }
                }
        });
        return false;
    });
});
</script>

</body>

</html>