(function($) {
  $(function() {
    $(".sidenav").sidenav();
    $(".parallax").parallax();
    $(".dropdown-trigger").dropdown();
    $(".tabs").tabs({
      swipeable: true,
      responsiveThreshold: 1920
    });
    $('.modal').modal();
    $('.tooltipped').tooltip();
    $('input, textarea').characterCounter();
    $('select').formSelect();

    $(".carousel").css({ height: "570px" });

    $('.chips').chips();
    $('.chips-initial').chips({
      data: [{
        tag: 'Apple',
      }, {
        tag: 'Microsoft',
      }, {
        tag: 'Google',
      }],
    });


    $('.chips-placeholder').chips({
      placeholder: 'Enter a tag',
      secondaryPlaceholder: '+Tag',
    });
    $('.chips-autocomplete').chips({
      autocompleteOptions: {
        data: {
          'Apple': null,
          'Microsoft': null,
          'Google': null
        },
        limit: Infinity,
        minLength: 1
      }
    });




    //validação
    $('.cpf').mask('000.000.000-00', {reverse: true});

  }); // end of document ready
})(jQuery); // end of jQuery name space
