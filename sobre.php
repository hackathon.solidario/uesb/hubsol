<?php include "resources/header.php"; ?>

  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

        <!-- Section: Features v.2 -->
        <section class="my-5 p-1">

        <!-- Section heading -->
        <h2 class="h1-responsive font-weight-bold text-center my-5">O que é o SolHuba?</h2>
        <!-- Section description -->
        <p class="lead grey-text text-justify w-responsive mx-auto mb-5">
        Plataforma web mobile para construção de redes de solidariedade com responsabilidade social, desenvolvida pelo CPDS/Fábrica de Software com o objetivo de promover e criar pontes entre Organizações da Sociedade Civil (OSCs), instituições públicas e privadas e a comunidade em geral.
        <br>
        O Sol, símbolo de luz e vida que irradia sentimentos e valores para construção de uma consciência solidária com inclusão social e sentido de pertença para os autores envolvidos. A palavra Hub traz consigo o significado de conexões, laços, pontes que fazem emergir o sentido de colaboração, parcerias, solidariedade.  
        </p>

        <!-- Grid row -->
        <div class="row">

        <!-- Grid column -->
        <div class="col-md-6 mb-md-0 mb-5">

            <!-- Grid row -->
            <div class="row">

            <!-- Grid column -->
            <div class="col-lg-2 col-md-3 col-2">
                <i class="fas fa-hands-helping blue-text fa-2x"></i>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-lg-10 col-md-9 col-10">
                <h4 class="font-weight-bold">Missão</h4>
                <p class="grey-text">
                    Promover o engajamento permanente de voluntários e estabelecimento de parcerias com instituições públicas e empresas privadas de forma a contribuir para a sustentabilidade econômica, financeira e social de OSCs. 
                </p>
            </div>
            <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-6 mb-md-0 mb-5">

            <!-- Grid row -->
            <div class="row">

            <!-- Grid column -->
            <div class="col-lg-2 col-md-3 col-2">
                <i class="fas fa-users blue-text fa-2x"></i>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-lg-10 col-md-9 col-10">
                <h4 class="font-weight-bold">Visão</h4>
                <p class="grey-text">
                    Inspirar e sensibilizar pessoas, entidades, empresas e comunidades para a construção, a partir da tecnologia, de uma rede de solidariedade
                </p>
            </div>
            <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Grid column -->


        </div>
        <!-- Grid column -->

        </div>
        <!-- Grid row -->

        </section>
        <!-- Section: Features v.2 -->

    </div>
  </main>
  <!--Main layout-->

  <?php include "resources/footer.php"; ?>