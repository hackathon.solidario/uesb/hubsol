<?php include "resources/header.php"; ?>

  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
        <!--Title-->
  <h2 class="section-heading">
    Atividades disponíveis
  </h2>
    <br><br>

  <!--Section: Live preview-->
  <section>

    <!-- Grid row -->
    <div class="row mx-1">

      <!-- Grid column -->
      <div class="col-md-12">

        <!-- Card deck -->
        <div class="card-deck">

          <!-- Card -->
          <div class="card mb-4">

            <!--Card image-->
            <div class="view overlay">
              <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/16.jpg" alt="Card image cap">
              <a href="#!">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>

            <!--Card content-->
            <div class="card-body">

              <!--Title-->
              <h4 class="card-title">Título da atividade</h4>
              <!--Text-->
              <p><b>Tipo da atividade:</b> Tipo da atividade</p>
              <p class="card-text">Descrição da atividade</p>
              <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
              <button type="button" class="btn btn-blue btn-md">Candidate-se</button>

            </div>

          </div>
          <!-- Card -->

          <!-- Card -->
          <div class="card mb-4">

            <!--Card image-->
            <div class="view overlay">
              <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/14.jpg" alt="Card image cap">
              <a href="#!">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>

            <!--Card content-->
            <div class="card-body">

              <!--Title-->
              <h4 class="card-title">Título da atividade</h4>
              <!--Text-->
              <p><b>Tipo da atividade:</b> Tipo da atividade</p>
              <p class="card-text">Descrição da atividade</p>
              <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
              <button type="button" class="btn btn-blue btn-md">Candidate-se</button>

            </div>

          </div>
          <!-- Card -->

          <!-- Card -->
          <div class="card mb-4">

            <!--Card image-->
            <div class="view overlay">
              <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/15.jpg" alt="Card image cap">
              <a href="#!">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>

             <!--Card content-->
             <div class="card-body">

                <!--Title-->
                <h4 class="card-title">Título da atividade</h4>
                <!--Text-->
                <p><b>Tipo da atividade:</b> Tipo da atividade</p>
                <p class="card-text">Descrição da atividade</p>
                <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                <button type="button" class="btn btn-blue btn-md">Candidate-se</button>

            </div>

          </div>
          <!-- Card -->

        </div>
        <!-- Card deck -->

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </section>
  <!--Section: Live preview-->
    </div>
  </main>
  <!--Main layout-->

  <?php include "resources/footer.php"; ?>