<?php
  include('resources/header.php');
?>

  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
    <p class="h4 mb-4">Listar Produtos</p>
    
    </div>
    <a href='cadastrar_produto.php' class="btn btn-success " data-titulo='Cadastrar Ação'><i class="la la-plus" ></i>+ Adicionar</a>

    <div class="responsive row" id="tabela">
        <table class="table table-bordered table-hover" id="datatable">
            <thead class="thead-default thead-lg">
                <tr>
                <th>#</th>
                <th>Título</th>
                <th>Sol(pontuação)</th>
                <th>Status</th>
                <th WIDTH=30px></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
  </main>
  <!--Main layout-->
<?php
  include('resources/footer.php');
?>
 <script type="text/javascript" src="font/dataTables/datatables.min.js"></script>
  <script type="text/javascript" src="font/toastr/toastr.min.js"></script>
  <script>
    toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "200",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
  </script>
<script>

 $(function () {
// configuração do dataTable
    $('#datatable').DataTable({
        "processing": true,
        "ajax": {
        "url": "include/produto.php",
        "type": "POST",
        "data":{consultarProduto:fk_usuario}
    },
        "columns": [
            { "data": "id" },
            { "data": "titulo" },
            { "data": "pontuacao" },
            { "data": "status" },
            { "data": "opcoes" }
        ],
        "language": {
                "url": "font/dataTables/Portuguese-Brasil.json"
        },
        pageLength: 10,
        fixedHeader: true,
        responsive: true,
        "sDom": 'rtip',
        columnDefs: [{
            targets: 'no-sort',
            orderable: false
        }]
    });
   });   
</script>

</body>

</html>