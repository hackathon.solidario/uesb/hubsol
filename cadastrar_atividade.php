<?php
  include('resources/header.php');
?>
  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
    <p class="h4 mb-4">Cadastrar Ação</p>
    <form class="text-center">
    <img src="img/placeholder.png" class="img-fluid mb-4" width="200rem" >
        
    <div class="row mb-4">
        <div class="col-4"></div>
        <div class="col-4">
             <div class="input-group">
                <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupFileAddon01">Enviar</span>
            </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="foto" name="foto"
            aria-describedby="inputGroupFileAddon01">
            <label class="custom-file-label" for="inputGroupFile01">selecionar</label>
        </div>
        </div>
        </div>
        <div class="col-4"></div>
</div>

<div class="form-row">
    <div class="form-group col-12 col-md-4">
      <input type="text" id="nome" name="nome" class="form-control" placeholder="E-mail">
    </div>
     <div class="form-group col-6 col-md-2">
      <input type="text" id="pontos" name="pontos" class="form-control" placeholder="E-mail">
    </div>
    <div class="form-group col-6 col-md-2">
      <input type="text" id="quantidade" name="quantidade" class="form-control" placeholder="E-mail">
    </div>
    <div class="form-group col-6 col-md-4">
     <select id="tipo" name="tipo" class="custom-select ">
            <option value='continua'>Ação contínua</option>
            <option value="periodica">Ação periódica</option>
          </select>
    </div>
</div>

<div class="form-row mb-4">
  <div class="col-lg-6">
        <input type="number" id="quantidade" name="quantidade" class="form-control" placeholder="Quantidade de vagas">
    </div>

    <div class="col-lg-6">
    <div class="form-row mb-4">
          
    </div>
</div>
</div>

<div class="form-row mb-4">
  <div class="col-lg-12">
  <textarea id="descricao" name="descricao" class="form-control" placeholder="Descrição"></textarea>
    </div>
</div>


<!-- Sign up button -->
<button class="btn btn-info my-4 btn-block" type="submit">Continuar</button>

<hr>

<!-- Terms of service -->
<p>Ao clicar em
    <em>Continuar</em> você concorda com os
    <a href="" target="_blank">termos de uso.</a>

</form>
    </div>

  </main>
  <!--Main layout-->
<?php
  include('resources/footer.php');
?>

</body>

</html>