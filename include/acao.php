<?php
session_start();
include '../database/BancoConexao.php';
$usuario = filter_var($_SESSION['id_cadastro'], FILTER_SANITIZE_NUMBER_INT);
if (isset($_POST['titulo'])) {
    
   if($_POST['imagem']==""){
        $imagem="placeholder.png";
    }else{
        $imagem = str_replace("\"", "",$_POST['imagem']);
    }

    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    $titulo =filter_var($_POST['titulo'], FILTER_SANITIZE_STRING);
    $descricao =filter_var($_POST['descricao'], FILTER_SANITIZE_STRING);
    $tipo_acao =filter_var($_POST['tipo'], FILTER_SANITIZE_STRING);
    $quantidade = filter_var($_POST['quantidade'], FILTER_SANITIZE_NUMBER_INT);
    $data_final =filter_var($_POST['data_final'], FILTER_SANITIZE_STRING);
    $data_inicial =filter_var($_POST['data_inicial'], FILTER_SANITIZE_STRING);
    $status ="ATIVO";
    
if ($id==""){

            $cad_acao = $con->prepare("INSERT INTO acao(fk_usuario,titulo,descricao,tipo_acao,imagem,
            quantidade,data_final,data_inicial,status) 
            VALUES('$usuario','$titulo','$descricao','$tipo_acao','$imagem','$quantidade',
            '$data_final','$data_inicial','ATIVO')");
            $retornoAcao = $cad_acao->execute();
}else {
            $cad_acao = $con->prepare("UPDATE acao SET titulo='$titulo',descricao='$descricao',
            tipo_acao='$tipo_acao',imagem='$imagem',quantidade='$quantidade',data_final='$data_final',
            data_inicial='$data_inicial'
            WHERE id='$id'");

            $retornoAcao = $cad_acao->execute();
}
    if ($retornoAcao) {
        echo json_encode('OK');
    } else {
        echo json_encode('ERRO');
    }
  }

if(isset($_POST['consultarAcao'])){

$fk_usuario = filter_var($_POST['consultarAcao'], FILTER_SANITIZE_NUMBER_INT);
    
  $request=$_REQUEST;
  $sql = $con->query("SELECT id,titulo,tipo,quantidade,status FROM acao WHERE fk_usuario='$fk_usuario'");
  $opcoes=""; 
  $totalData=$sql->rowCount();
  $totalFilter=$totalData;
  $data=array();
  
    while ($row = $sql->fetch(PDO::FETCH_OBJ)) {
        $opcoes="";
     $subdata=array("id"=>$row->id,
                    "titulo"=>$row->titulo,
                    "tipo"=>$row->tipo,
                    "quantidade"=>$row->quantidade,
                    "status"=>$row->status,
                    "opcoes"=>$opcoes);
     $data[]=$subdata;
 
    }
    echo json_encode(array(
                "draw"              =>  intval($request['draw']),
                "recordsTotal"      =>  intval($totalData),
                "recordsFiltered"   =>  intval($totalFilter),
                "data"              =>   $data
            ));
}  

 

