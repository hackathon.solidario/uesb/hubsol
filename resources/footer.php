  <!--Footer-->
  <footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4 wow fadeIn">

    <!--Call to action-->
    <!-- <div class="pt-4">
      <a class="btn btn-outline-white" href="https://mdbootstrap.com/docs/jquery/getting-started/download/"
        target="_blank" role="button">Saiba mais

        <i class="fas fa-download ml-2"></i>
      </a>
      <a class="btn btn-outline-white" href="https://mdbootstrap.com/education/bootstrap/" target="_blank"
        role="button">Veja nossos tutoriais
        <i class="fas fa-graduation-cap ml-2"></i>
      </a>
    </div> -->
    <!--/.Call to action-->

    <!-- <hr class="my-4"> -->

    <!-- Social icons -->
    <!-- <div class="pb-4">
      <a href="#" target="_blank">
        <i class="fab fa-facebook-f mr-3"></i>
      </a>

      <a href="#" target="_blank">
        <i class="fab fa-instagram mr-3"></i>
      </a>

      <a href="#" target="_blank">
        <i class="fab fa-youtube mr-3"></i>
      </a>

      
    </div> -->
    <!-- Social icons -->

    <!--Copyright-->
    <div class="footer-copyright py-3">
      © 2019 Copyright:
      <br><br>
      <a href="#" target="_blank">
        <img src="img/proex.png" alt="UESB - Proex" style="max-height: 70px; padding: 5px;">
      </a>
      <a href="#" target="_blank">
        <img src="img/cpds.png" alt="CPDS - Fábrica de Softare" style="max-height: 70px; padding: 5px;">
      </a>
    </div>
    <!--/.Copyright-->

  </footer>
  <!--/.Footer-->
 
  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.4.0.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <script type="text/javascript" src="js/acoes.js"></script>
   
 
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();
 
  </script>
 