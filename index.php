<?php include "resources/header.php"; 
  require('resources/componentes/card.php');
  include "database/BancoConexao.php";
?>

  <!--Main layout-->
  <main class="pt-5 mx-lg-5">

    

    <div class="container-fluid mt-5">

        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img class="d-block w-100" src="img/banner1.png"
                    alt="First slide">
                </div>
                <!-- <div class="carousel-item">
                <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(16).jpg"
                    alt="Second slide">
                </div>
                <div class="carousel-item">
                <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(17).jpg"
                    alt="Third slide">
                </div> -->
            </div>
            <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <br><br>

        <h2 class="section-heading" style="text-align: center;">
            Últimas ações disponíveis
        </h2>

        <!-- Card -->
        <?php
        $sql = $con->query("SELECT id, titulo, descricao, imagem, quantidade, status FROM acao WHERE status = 'ATIVO' ORDER BY id;");

        while ($acao = $sql->fetch(PDO::FETCH_OBJ)) {
          $titulo = $acao->titulo;
          $imagem = $acao->imagem;
          $descricao = $acao->descricao;
          $id = $acao->id;
          $data_inicial = $acao->data_inicial;
          $quantidade = $acao->quantidade;

        
          //gerarCardsAcoes(titulo, img, imgong, descricao, link, data, quantidade de vagas){
          gerarCardsAcoes($titulo, 'img/'.$imagem, 'img/user.jpg', $descricao, $id, $data_inicial, $quantidade);
        }
      ?>
        <!-- Card -->

    </div>
  </main>
  <!--Main layout-->

  <?php include "resources/footer.php"; ?>