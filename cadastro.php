
<?php
  include('resources/header.php');
?>
  
  <!--Main Navigation-->

  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
<!-- Default form register -->
<form class="text-center" id="formUsuario" method="POST">

    <p class="h4 mb-4">Novo Usuário</p>

<div class="form-row">
    <div class="form-group col-12 col-md-4">
      <select id='tipo_usuario' name='tipo_usuario' class="custom-select" onchange='verifica_tipo();'>
        <option value=''>Selecione</option>
        <option value='voluntário'>Voluntario</option>
        <option value="empresa">Empresa</option>
        <option value="instituição">Instituição Pública</option>
        <option value="osc">OSC</option>
      </select>
    </div> 
</div>
<div class="form-row " id='voluntario' hidden>
    <div class="form-group col-12 col-md-8">
      <input type="text" id="nome_voluntario" name="nome_voluntario" class="form-control" placeholder="Nome e Sobrenome">
    </div>
    <div class="form-group col-12 col-md-4">
      <input type="text" id="cpf_voluntario" name="cpf_voluntario" class="form-control" placeholder="CPF">
    </div>
    <div class="form-group col-12 col-md-4">
      <input type="text" id="area_interesse_voluntario" name="area_interesse_voluntario" class="form-control" placeholder="Áreas de Interesse">
     </div>
</div> 
  <div class="form-row" id='empresa' hidden>
     <div class="form-group col-12 col-md-8">
      <input type="text" id="nome_fantasia" name="nome_fantasia" class="form-control" placeholder="Nome Fantasia">
    </div>
    <div class="form-group col-12 col-md-4">
      <input type="text" id="cnpj" name="cnpj" class="form-control" placeholder="CNPJ">
    </div>
    <div class="form-group col-12 col-md-4">
      <input type="text" id="area_insteresse_empresa" name="area_insteresse_empresa" class="form-control" placeholder="Área de Interesse">
    </div>
</div> 
  <div class="form-row" id='instituicao' hidden>
    <div class="form-group col-12 col-md-6">
      <input type="text" id="nome_instituicao" name="nome_instituicao" class="form-control" placeholder="Nome">
    </div>
    <div class="form-group col-12 col-md-2">
      <input type="text" id="sigla" name="sigla" class="form-control" placeholder="Sigla">
    </div>
    <div class="form-group col-12 col-md-4">
      <input type="text" id="cnpj_instituicao" name="cnpj_instituicao" class="form-control" placeholder="Cnpj">
    </div>
</div> 
  <div class="form-row" id='osc' hidden>
    <div class="form-group col-12  col-md-6">
      <input type="text" id="nome_osc" name="nome_osc" class="form-control" placeholder="Nome">
    </div>
    <div class="form-group col-12 col-md-2">
      <input type="text" id="sigla_osc" name="sigla_osc" class="form-control" placeholder="CPF">
    </div>
    <div class="form-group col-12 col-md-4">
      <input type="text" id="cnpj_osc" name="cnpj_osc" class="form-control" placeholder="CNPJ">
    </div>
    <div class="form-group col-12 col-md-4">
      <input type="text" id="area_atuacao" name="area_atuacao" class="form-control" placeholder="Área de Atuação">
    </div>
</div> 
<div class="form-row ">
    <div class="form-group col-6 col-md-4">
      <input type="text" id="cidade" name="cidade" class="form-control" placeholder="Cidade">
    </div>
    <div class="form-group col-6 col-md-4">
      <input type="text" id="estado" name="estado" class="form-control" placeholder="Estado">
    </div>
     <div class="form-group col-12 col-md-4">
      <input type="text" id="endereco" name="endereco" class="form-control" placeholder="Logradouro, Nº, Complemento">
    </div>
    <div class="form-group col-12 col-md-4">
      <input type="text" id="telefone" name="telefone" class="form-control" placeholder="Telefone">
    </div>
    <div class="form-group col-6 col-md-4">
      <input type="email" id="email" name="email" class="form-control" placeholder="E-mail">
    </div>
    <div class="form-group col-6 col-md-4">
      <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha">
    </div>
</div>
    <!-- Sign up button -->
    <button class="btn btn-info btn-block col-12 col-md-2" type="submit">Cadastrar</button>
    <hr>

    <!-- Terms of service -->
    <p>Ao clicar em
        <em>Cadastrar</em> você concorda com os
        <a href="" target="_blank">termos de uso.</a>

</form>
<!-- Default form register -->
    </div>

  </main>
  <!--Main layout-->
<?php
  include('resources/footer.php');
?>
<script type="text/javascript" src="font/toastr/toastr.min.js"></script>
 <script>
    toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "200",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
  </script>
<script>
function verifica_tipo() {
    var tipo = $("#tipo_usuario").val();
    if(tipo=="voluntário"){
      $("#voluntario").removeAttr('hidden');
      $("#empresa").attr('hidden');
      $("#instituicao").attr('hidden');
      $("#osc").attr('hidden');
    }else if(tipo=="empresa"){
      $("#voluntario").attr('hidden');
      $("#empresa").removeAttr('hidden');
      $("#instituicao").attr('hidden');
      $("#osc").attr('hidden');
    }else if(tipo=="instituição"){
      $("#voluntario").attr('hidden');
      $("#empresa").attr('hidden');
      $("#instituicao").removeAttr('hidden');
      $("#osc").attr('hidden');
    }else if(tipo=="osc"){
      $("#voluntario").attr('hidden');
      $("#empresa").attr('hidden');
      $("#instituicao").attr('hidden');
      $("#osc").removeAttr('hidden');

    }
}  

//função para salvar formulário 
$(document).ready(function () {
    $('#formUsuario').submit(function() {
        var dados = $('#formUsuario').serialize();
        $.ajax({
                type: 'POST',
                dataType: 'json',
                url: 'include/usuario.php',
                async: true,
                data: dados,
                success: function(data) {
                    if(data=="OK"){
                        toastr.success('Sucesso - operação realizada!');
                        document.location.href='login.php';
                    }else if(data=="ERRO"){
                        toastr.error('Erro - Não foi possível realizar operação.!');
                    }
                }
        });
        return false;
    });
});
</script>
</body>

</html>