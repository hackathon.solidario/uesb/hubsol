<?php
  include('resources/header.php');
?>
  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
    <p class="h4 mb-4">Cadastrar Ação</p>
    <form class="text-center" id="formAcao" method="POST">
    <img src="img/placeholder.png" class="img-fluid mb-4" width="200rem" >
        
    <div class="row mb-4">
        <div class="col-4"></div>
        <div class="col-4">
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="imagem" name="imagem"
            aria-describedby="inputGroupFileAddon01">
            <label class="custom-file-label" for="inputGroupFile01">selecionar</label>
        </div>
        </div>
        </div>
        <div class="col-4"></div>
</div>

<div class="form-row">
    <div class="form-group col-12 col-md-6">
      <input type="text" id="titulo" name="titulo" class="form-control" placeholder="Título da Ação">
    </div>
    <div class="form-group col-6 col-md-3">
     <select id="tipo" name="tipo" class="custom-select" onchange='verifica_tipo();'>
            <option value='continua' Selected>Ação contínua</option>
            <option value="esporadica">Ação Esporádica</option>
          </select>
    </div>
     <div class="form-group col-6 col-md-3">
      <input type="text" id="quantidade" name="quantidade" class="form-control" placeholder="Quantidade de Voluntários">
    </div>
</div>
<div class="form-row" id='esporadica' hidden>
    <div class="form-group col-12 col-md-6">
      <input type="date" id="data_inicial" name="data_inicial" class="form-control" placeholder="Título da Ação">
    </div>
    <div class="form-group col-12 col-md-6">
      <input type="date" id="data_final" name="data_final" class="form-control" placeholder="Título da Ação">
    </div>
</div>
<div class="form-row">
    <div class="form-group col-12 col-md-12">
        <textarea type="text" id="descricao" name="descricao" class="form-control" placeholder="Descrição da Ação"></textarea>
    </div>
</div>

<!-- Sign up button -->
<button class="btn btn-success btn-block col-12 col-md-2" type="submit">Salvar</button>
<hr>
</form>
    </div>

  </main>
  <!--Main layout-->
<?php
  include('resources/footer.php');
?>
<script type="text/javascript" src="font/toastr/toastr.min.js"></script>
 <script>
    toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "200",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
  </script>
<script>
function verifica_tipo() {
    var tipo = $("#tipo").val();
    if(tipo=="continua"){
      $("#esporadica").attr('hidden');
    }else if(tipo=="esporadica"){
      $("#esporadica").removeAttr('hidden');
    }
}  

//função para salvar formulário 
$(document).ready(function () {
    $('#formAcao').submit(function() {
        var dados = $('#formAcao').serialize();
        $.ajax({
                type: 'POST',
                dataType: 'json',
                url: 'include/acao.php',
                async: true,
                data: dados,
                success: function(data) {
                    if(data=="OK"){
                        toastr.success('Sucesso - operação realizada!');
                    }else if(data=="ERRO"){
                        toastr.error('Erro - Não foi possível realizar operação.!');
                    }
                }
        });
        return false;
    });
});
</script>

</body>

</html>