<!-- Dropdown Structure -->
<!-- <ul id="dropdown1" class="dropdown-content">
    <li><a href="#!" class="pink-text">Categoria</a></li>
    <li><a href="#!" class="pink-text">Categoria</a></li>
    <li class="divider"></li>
    <li><a href="#!" class="pink-text">Categoria</a></li>
</ul> -->

<nav class="blue darken-4 white-text" role="navigation">
    <div class="nav-wrapper container">
        <div class="d-flex logo-posicao"><a href="index.php" class="brand-logo white-text"><img src="imgs/logo.png"
                    class="responsive-img logo-fix"></a></div>
        <!-- <ul class="right hide-on-med-and-down white-text">
            <li><a href="sass.html" class="white-text">Top</a></li>
            <li><a href="badges.html" class="white-text">Aleatorio</a></li>
            <li>
                <a class="dropdown-trigger white-text" href="#!" data-target="dropdown1">Categorias<i
                        class="material-icons right">arrow_drop_down</i></a>
            </li>
        </ul> -->

        <!-- <ul id="nav-mobile" class="sidenav">
            <li style="color: black;">Sol HUB</li>
            <li><a href="#">Atividades</a></li>
            <li><a href="#">Loja</a></li>
            <li><a href="#">Parceiros</a></li>
            <li><a href="#">Sobre</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a> -->

        <ul id="slide-out" class="sidenav">
            <li>
                <div class="user-view">
                <div class="background">
                    <img src="imgs/bluebackground.png">
                </div>
                <a href="#user"><img class="circle" src="imgs/user.jpg"></a>
                <a href="#name"><span class="white-text name">Nome do usuário</span></a>
                <a href="#email"><span class="white-text email">usuario@email.com</span></a>
                </div>
            </li>
            <li><a class="waves-effect" href="#">Atividade</a></li>
            <li><a class="waves-effect" href="loja.php">Loja</a></li>
            <li><a class="waves-effect" href="#">Parceiros</a></li>
            <li><a class="waves-effect" href="sobre.php">Sobre</a></li>
            </ul>
            <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>