-- phpMyAdmin SQL Dump
-- version 3.3.3
-- http://www.phpmyadmin.net
--
-- Servidor: mysql06-farm13.kinghost.net
-- Tempo de Geração: Mai 05, 2019 as 12:48 PM
-- Versão do Servidor: 5.5.43
-- Versão do PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `greenestudio20`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acao`
--

CREATE TABLE IF NOT EXISTS `acao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) DEFAULT NULL,
  `titulo` text,
  `descricao` text,
  `tipo_acao` varchar(45) DEFAULT NULL,
  `imagem` varchar(45) DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `data_inicial` date DEFAULT NULL,
  `data_final` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `acao`
--

INSERT INTO `acao` (`id`, `fk_usuario`, `titulo`, `descricao`, `tipo_acao`, `imagem`, `quantidade`, `data_inicial`, `data_final`, `status`) VALUES
(1, 0, 'Título da ação', 'Aqui vai todas as informações sobre a ação.', '', 'placeholder.png', 10, '0000-00-00', '0000-00-00', 'ATIVO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pontuacao`
--

CREATE TABLE IF NOT EXISTS `pontuacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) DEFAULT NULL,
  `pontos` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `pontuacao`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) DEFAULT NULL,
  `imagem` text NOT NULL,
  `titulo` text,
  `descricao` text,
  `pontuacao` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `fk_usuario`, `imagem`, `titulo`, `descricao`, `pontuacao`, `status`) VALUES
(1, 5, 'placeholder.png', 'Teste tulio', 'teste teasdasd ads', 12, 'ATIVO'),
(2, 5, 'placeholder.png', 'Teste tulio', 'teste teasdasd ads', 12, 'ATIVO'),
(3, 5, 'placeholder.png', 'Teste tulio', 'teste teasdasd ads', 12, 'ATIVO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` text,
  `tipo_usuario` varchar(50) DEFAULT NULL,
  `nome` text,
  `sigla` varchar(45) DEFAULT NULL,
  `email` text,
  `senha` text,
  `cpf_cnpj` varchar(45) DEFAULT NULL,
  `telefone_contato` varchar(45) DEFAULT NULL,
  `area_interesse` text,
  `cidade` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `endereco` text,
  `area_atuacao` varchar(45) DEFAULT NULL,
  `dados_bancarios` text NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `imagem`, `tipo_usuario`, `nome`, `sigla`, `email`, `senha`, `cpf_cnpj`, `telefone_contato`, `area_interesse`, `cidade`, `estado`, `endereco`, `area_atuacao`, `dados_bancarios`, `status`) VALUES
(1, '', 'voluntário', 'jessica cerqueira santos', '', 'jessicagreig@hotmail.com', 'd43a3cbf93c9d484c2b7d9ded04c45d2', '', '', 'teste', '', '', NULL, '', '', 'ATIVO'),
(2, 'placeholder.png', 'voluntário', 'Tailane Alves Lima Maia', '', 'tailane.maia@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 'Saúde', '', '', NULL, '', '', 'ATIVO'),
(3, 'placeholder.png', 'voluntário', 'Tailane Alves Lima Maia', '', 'tailane.maia@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 'Saúde', '', '', NULL, '', '', 'ATIVO'),
(4, 'placeholder.png', 'voluntário', 'jebvebvvv', '', 'tailane.maia@gmail.com', '4297f44b13955235245b2497399d7a93', '', '', 'Saúde', '', '', NULL, '', '', 'ATIVO'),
(5, 'placeholder.png', 'osc', 'biegbnwpinepo', '000000', 'tailane.maia@gmail.com', '4297f44b13955235245b2497399d7a93', '', '', '00000000', '', '', NULL, 'kndkvandbf', '', 'ATIVO'),
(6, 'placeholder.png', 'osc', 'biegbnwpinepo', '000000', 'tailane.maia@gmail.com', '4297f44b13955235245b2497399d7a93', '', '', '00000000', '', '', NULL, 'kndkvandbf', '', 'ATIVO'),
(7, 'placeholder.png', 'osc', 'biegbnwpinepo', '000000', 'a@gmail.com', '4297f44b13955235245b2497399d7a93', '', '', '00000000', '', '', NULL, 'kndkvandbf', '', 'ATIVO'),
(8, 'placeholder.png', 'empresa', 'nvjbavsdjk', '', 'a@gmail.com', '4297f44b13955235245b2497399d7a93', '', '', 'jbvhegwbgejr', '', '', NULL, '', '', 'ATIVO'),
(9, 'placeholder.png', 'instituição', '', '', 'tailane.maia@gmail.com', '4297f44b13955235245b2497399d7a93', '', '', '', '', '', NULL, '', '', 'ATIVO'),
(10, 'placeholder.png', 'instituição', '', '', 'tailane@gmail.com', '4297f44b13955235245b2497399d7a93', '', '', '', '', '', NULL, '', '', 'ATIVO'),
(11, 'placeholder.png', 'instituição', '', '', 'tailane@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', '', '', NULL, '', '', 'ATIVO'),
(12, 'placeholder.png', 'osc', 'Tailane Alves Lima Maia', '000000', 'tailane.maia@gmail.com', '4297f44b13955235245b2497399d7a93', '', '', '00000000', '', '', NULL, 'kndkvandbf', '', 'ATIVO'),
(13, 'placeholder.png', 'osc', 'Tailane Alves Lima Maia', '000000', 't@gmail.com', '4297f44b13955235245b2497399d7a93', '', '', '00000000', '', '', NULL, 'kndkvandbf', '', 'ATIVO'),
(14, 'placeholder.png', 'voluntário', 'jessica cerqueira santos', '', 'jessicagreig@hotmail.com', 'd43a3cbf93c9d484c2b7d9ded04c45d2', '', '', 'teste', '', '', NULL, '', '', 'ATIVO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_acao`
--

CREATE TABLE IF NOT EXISTS `usuario_acao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) DEFAULT NULL,
  `fk_acao` int(11) DEFAULT NULL,
  `data_hora` timestamp NULL DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `usuario_acao`
--

