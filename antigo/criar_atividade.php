<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
    <title>Sol HUB - Cadastro</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" />
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet" />
</head>

<body>
    <?php include "include/header.php"; ?>

    <!-- modal -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Selecione sua foto</h4>
            <br>
            <div class="file-field input-field">
                <div class="btn">
                    <span>Buscar</span>
                    <input type="file" placeholder="Selecione sua foto">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Enviar</a>
        </div>
    </div>


    <div class="container">
        <div class="section">
            <div class="row">
                <h2 class="font-per-light center-align mt-0">Cadastro de Atividades</h2>
            </div>
            <div class="row">
                <div class="col s12 m12 l12">
                    <a class="modal-trigger" href="#modal1"><img src="imgs/banner-esticado-placeholder.png"
                            class="center-align" width="100%"></a>
                    <label>
                        <p class="center-align mt-0">Tamanho maximo: 1000x500px</p>
                    </label>
                </div>

                <br>

            </div>
            <div class="row">
                <form class="col s12">
                    <div class="row">
                        <div class="input-field inline col s6">
                            <i class="material-icons prefix">assignment</i>
                            <input id="nome" type="text" class="validate" data-length="50">
                            <label class="font-per" for="nome">Nome da terefa<i style="color: rgb(245, 109, 109);">
                                    *</i></label>
                        </div>

                        <div class="input-field inline col s6">
                            <i class="material-icons prefix">casino</i>
                            <input id="pontos" type="number" data-length="50" class="validate">
                            <label class="font-per" for="pontos">Pontos<i style="color: rgb(245, 109, 109);">
                                    *</i></label>
                            <span class="helper-text">Quanto cada voluntario vai
                                ganhar após finalizar a atividade.</span>

                        </div>
                    </div>

                    <div class="row">
                        <form class="col s12">
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">art_track</i>
                                    <textarea id="textarea1" data-length="500" class="materialize-textarea"></textarea>
                                    <label for="textarea1">Descrição sobre a ação</label>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="row">
                        <div class="input-field inline col s12">
                            <i class="material-icons prefix">email</i>
                            <input class="font-per" id="email" type="text" class="validate">
                            <label class="font-per" for="email">Email<i style="color: rgb(245, 109, 109);">
                                    *</i></label>
                        </div>
                    </div>

                    <div class="row">
                        <label class="font-per">Habilidades<i style="color: rgb(245, 109, 109);"> *</i></label>
                        <div class="chips">
                            <input id="habilidades" class="custom-class">
                        </div>
                        <label class="font-per">(Após digitar, pressione enter para confirma e/ou inserir mais)</label>
                        <p>
                            <label>
                                <input id="termos-uso" type="checkbox" class="filled-in" />
                                <span class="font-per">Li e estou de acordo com os <a href="#">Termos de uso<i
                                            style="color: rgb(245, 109, 109);"> *</i></a></span>
                            </label>
                        </p>
                    </div>
                    <div class="d-flex justify-center">
                        <button id="btn-cadastro" class="btn blue darken-3 waves-effect waves-light disabled tooltipped"
                            type="submit" name="action" data-position="bottom"
                            data-tooltip="Clique para finalizar o seu cadastro">Cadastrar
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>





    <?php include "include/footer.php"; ?>

    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/validar_cadastro.js"></script>
</body>

</html>