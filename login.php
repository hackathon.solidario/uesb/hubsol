<?php
  include('resources/header.php');
  require('resources/componentes/card.php');
?>
  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
  <div class="container">
    <div class="row">
        <div class="col-lg-4">

  </div>
<form class="text-center border border-light p-5 col-lg-4  ">

    <center><img src="img/logo.png" class="img-fluid mb-4 wow pulse" width="90%"></center>
    
    <div class="md-form">
        <input type="email" id="email" class="form-control mb-4 wow fadeIn">
        <label for="materialLoginFormPassword">Email</label>
    </div>

    
<div class="md-form">
        <input type="password" id="senha" class="form-control">
        <label for="materialLoginFormPassword">Senha</label>
      </div>


    <div class="d-flex justify-content-around">
        <div>
            <!-- Remember me -->
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                <label class="custom-control-label wow fadeIn" for="defaultLoginFormRemember">Salvar</label>
            </div>
        </div>
        <script>
            function validar() {
                var usuario = document.getElementById('email').value;  
                var senha = document.getElementById('senha').value;

                $.post('include/validacao.php',
                {validarEntrada:'validarEntrada',usuario:usuario,senha:senha},
                function(data){
                    $('#resultadoLogin').html(data);
                });
            }
        </script>
        <div id="resultadoLogin"></div>
        <div>
            <!-- Forgot password -->
            <a href="" class="wow fadeIn">Esqueceu a senha?</a>
        </div>
    </div>

    <!-- Sign in button -->
    <button class="btn btn-info btn-block my-4 wow tada" type="button" onclick='validar();'>Entrar</button>

    <!-- Register -->
    <p class="wow fadeIn">Não tem cadastro?
        <a href="cadastro.php">Cadastre-se agora</a>
    </p>


</form>
<!-- Default form login -->     
  </div>
  <div class="col-4-lg">

  </div>
  </div>

  </main>

<?php
  include('resources/footer.php');
?>

</body>

</html>