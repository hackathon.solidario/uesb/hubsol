<?php
  include('resources/header.php');
  require('resources/componentes/card.php');
  include "database/BancoConexao.php";
?>
  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5 col-12">
    <p class="h4 mb-4">Ações disponiveis</p>
    <div class="row">
      <?php
        $sql = $con->query("SELECT id, titulo, descricao, imagem, quantidade, status FROM acao WHERE status = 'ATIVO' ORDER BY id;");

        while ($acao = $sql->fetch(PDO::FETCH_OBJ)) {
          $titulo = $acao->titulo;
          $imagem = $acao->imagem;
          $descricao = $acao->descricao;
          $id = $acao->id;
          $data_inicial = $acao->data_inicial;
          $quantidade = $acao->quantidade;

        
          //gerarCardsAcoes(titulo, img, imgong, descricao, link, data, quantidade de vagas){
          gerarCardsAcoes($titulo, 'img/'.$imagem, 'img/user.jpg', $descricao, $id, $data_inicial, $quantidade);
        }
      ?>
    </div>
        
  </div>

  </main>

<?php
  include('resources/footer.php');
?>

</body>

</html>