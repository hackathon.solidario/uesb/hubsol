<?php
  include('resources/header.php');
?>

  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
    <p class="h4 mb-4">Listar Ações</p>
    
    </div>
    <a href='cadastrar_acao.php' class="btn btn-success " data-titulo='Cadastrar Ação'><i class="la la-plus" ></i>+ Adicionar</a>

    <div class="responsive row" id="tabela">
        <table class="table table-bordered table-hover" id="datatable">
            <thead class="thead-default thead-lg">
                <tr>
                <th>#</th>
                <th>Título</th>
                <th>Tipo</th>
                <th>Quant. Voluntários</th>
                <th WIDTH=30px></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
  </main>
  <!--Main layout-->
<?php
  include('resources/footer.php');
?>
 <script type="text/javascript" src="font/dataTables/datatables.min.js"></script>
  <script type="text/javascript" src="font/toastr/toastr.min.js"></script>
  <script>
    toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "200",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
  </script>
<script>
    var fk_usuario = 1;
function verifica_tipo() {
    var tipo = $("#tipo").val();
    if(tipo=="continua"){
      $("#esporadica").attr('hidden');
    }else if(tipo=="esporadica"){
      $("#esporadica").removeAttr('hidden');
    }
}  

//função para salvar formulário 
$(document).ready(function () {
    $('#formAcao').submit(function() {
        var dados = $('#formAcao').serialize();
        $.ajax({
                type: 'POST',
                dataType: 'json',
                url: 'include/acao.php',
                async: true,
                data: dados,
                success: function(data) {
                    if(data=="OK"){
                        toastr.success('Sucesso - operação realizada!');
                    }else if(data=="ERRO"){
                        toastr.error('Erro - Não foi possível realizar operação.!');
                    }
                }
        });
        return false;
    });
});
 $(function () {
// configuração do dataTable
    $('#datatable').DataTable({
        "processing": true,
        "ajax": {
        "url": "include/acao.php",
        "type": "POST",
        "data":{consultarAcao:fk_usuario}
    },
        "columns": [
            { "data": "id" },
            { "data": "titulo" },
            { "data": "tipo" },
            { "data": "quantidade" },
            { "data": "status" },
            { "data": "opcoes" }
        ],
        "language": {
                "url": "font/dataTables/Portuguese-Brasil.json"
        },
        pageLength: 10,
        fixedHeader: true,
        responsive: true,
        "sDom": 'rtip',
        columnDefs: [{
            targets: 'no-sort',
            orderable: false
        }]
    });
   });   
</script>

</body>

</html>