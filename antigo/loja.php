<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
  <title>Sol HUB</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" />
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet" />

</head>

<body>
  <!-- Dropdown Structure -->
  <ul id="dropdown1" class="dropdown-content">
    <li style="color: black;">Sol HUB</li>
    <li><a href="#">Atividades</a></li>
    <li><a href="#">Loja</a></li>
    <li><a href="#">Parceiros</a></li>
    <li><a href="#">Sobre</a></li>
  </ul>

  <nav class="blue darken-4 white-text" role="navigation">
    <div class="nav-wrapper container">
      <a href="#!" class="brand-logo white-text"><img src="imgs/logo.png"
        class="responsive-img logo-fix"></a>
      <ul class="right hide-on-med-and-down white-text">
        <li><a href="sass.html" class="white-text">Top</a></li>
        <li><a href="badges.html" class="white-text">Aleatorio</a></li>
        <!-- Dropdown Trigger -->
        <li>
          <a class="dropdown-trigger white-text" href="#!" data-target="dropdown1">Categorias<i
              class="material-icons right">arrow_drop_down</i></a>
        </li>
      </ul>

      <!-- <ul id="nav-mobile" class="sidenav">
        <li style="color: black;">Sol HUB</li>
        <li><a href="#">Atividades</a></li>
        <li><a href="#">Loja</a></li>
        <li><a href="#">Parceiros</a></li>
        <li><a href="#">Sobre</a></li>
      </ul> 
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a> -->

      <ul id="slide-out" class="sidenav">
          <li>
            <div class="user-view">
              <div class="background">
                <img src="imgs/bluebackground.png">
              </div>
              <a href="#user"><img class="circle" src="imgs/user.jpg"></a>
              <a href="#name"><span class="white-text name">Nome do usuário</span></a>
              <a href="#email"><span class="white-text email">usuario@email.com</span></a>
            </div>
          </li>
          <li><a class="waves-effect" href="#">Atividade</a></li>
          <li><a class="waves-effect" href="#">Loja</a></li>
          <li><a class="waves-effect" href="#">Parceiros</a></li>
          <li><a class="waves-effect" href="#">Sobre</a></li>
        </ul>
        <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>


  <div class="container">
    <div class="section">
      <div class="row">
        <h2 class="font-per center-align">Loja de bonificações</h2>
      </div>
      <div class="row">
        <div class="col s6 m4">
          <div class="card">
            <div class="card-image">
              <img src="imgs/place.png" />
              <a class="btn-floating halfway-fab waves-effect waves-light  blue darken-3 darken-1 pulse"><i
                  class="material-icons">chevron_right</i></a>
            </div>
            <div class="card-content">
              <span class="card-title">Título do produto</span>
              <h6><b>Descrição</b></h6>
              <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit quaerat ratione, illum velit iure esse dolore aliquid.
              </p>
              <h6><b>Instituição:</b> Nome da Instituição</h6>
              <h6><b>Vale:</b> 100 pts</h6>
            </div>
          </div>
        </div>

        <div class="col s6 m4">
          <div class="card">
            <div class="card-image">
              <img src="imgs/place.png" />
              <a class="btn-floating halfway-fab waves-effect waves-light  blue darken-3 darken-1 pulse"><i
                  class="material-icons">chevron_right</i></a>
            </div>
            <div class="card-content">
              <span class="card-title">Título do produto</span>
              <h6><b>Descrição</b></h6>
              <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit quaerat ratione, illum velit iure esse dolore aliquid.
              </p>
              <h6><b>Instituição:</b> Nome da Instituição</h6>
              <h6><b>Vale:</b> 100 pts</h6>
            </div>
          </div>
        </div>

        <div class="col s6 m4">
          <div class="card">
            <div class="card-image">
              <img src="imgs/place.png" />
              <a class="btn-floating halfway-fab waves-effect waves-light  blue darken-3 darken-1 pulse"><i
                  class="material-icons">chevron_right</i></a>
            </div>
            <div class="card-content">
              <span class="card-title">Título do produto</span>
              <h6><b>Descrição</b></h6>
              <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit quaerat ratione, illum velit iure esse dolore aliquid.
              </p>
              <h6><b>Instituição:</b> Nome da Instituição</h6>
              <h6><b>Vale:</b> 100 pts</h6>
            </div>
          </div>
        </div>

        <div class="col s6 m4">
          <div class="card">
            <div class="card-image">
              <img src="imgs/place.png" />
              <a class="btn-floating halfway-fab waves-effect waves-light  blue darken-3 darken-1 pulse"><i
                  class="material-icons">chevron_right</i></a>
            </div>
            <div class="card-content">
              <span class="card-title">Título do produto</span>
              <h6><b>Descrição</b></h6>
              <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit quaerat ratione, illum velit iure esse dolore aliquid.
              </p>
              <h6><b>Instituição:</b> Nome da Instituição</h6>
              <h6><b>Vale:</b> 100 pts</h6>
            </div>
          </div>
        </div>

        <div class="col s6 m4">
          <div class="card">
            <div class="card-image">
              <img src="imgs/place.png" />
              <a class="btn-floating halfway-fab waves-effect waves-light  blue darken-3 darken-1 pulse"><i
                  class="material-icons">chevron_right</i></a>
            </div>
            <div class="card-content">
              <span class="card-title">Título do produto</span>
              <h6><b>Descrição</b></h6>
              <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit quaerat ratione, illum velit iure esse dolore aliquid.
              </p>
              <h6><b>Instituição:</b> Nome da Instituição</h6>
              <h6><b>Vale:</b> 100 pts</h6>
            </div>
          </div>
        </div>

        <div class="col s6 m4">
          <div class="card">
            <div class="card-image">
              <img src="imgs/place.png" />
              <a class="btn-floating halfway-fab waves-effect waves-light  blue darken-3 darken-1 pulse"><i
                  class="material-icons">chevron_right</i></a>
            </div>
            <div class="card-content">
              <span class="card-title">Título do produto</span>
              <h6><b>Descrição</b></h6>
              <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit quaerat ratione, illum velit iure esse dolore aliquid.
              </p>
              <h6><b>Instituição:</b> Nome da Instituição</h6>
              <h6><b>Vale:</b> 100 pts</h6>
            </div>
          </div>
        </div>

      </div>



      <?php include "include/footer.php"; ?>

      <!--  Scripts-->
      <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
      <script src="js/init.js"></script>

      <script>
         document.addEventListener('DOMContentLoaded', function() {
          var elems = document.querySelectorAll('.slider');
          var instances = M.Slider.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function(){
          $('.slider').slider();
        });
      </script>
</body>

</html>