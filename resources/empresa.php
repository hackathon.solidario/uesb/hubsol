<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Sol Hub</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.min.css" rel="stylesheet">
  <link href="font/dataTables/datatables.min.css" rel="stylesheet" />
  <link href="font/toastr/toastr.css" rel="stylesheet" />
  <style>
    .map-container {
      overflow: hidden;
      padding-bottom: 56.25%;
      position: relative;
      height: 0;
    }

    .map-container iframe {
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      position: absolute;
    }
  </style>
</head>

<body class="grey lighten-3">

  <!--Main Navigation-->
  <header>
    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg d-lg-none navbar-light white scrolling-navbar">
      <div class="container-fluid">

        <!-- Brand -->
        <a class="navbar-brand waves-effect" href="index.php">
          <img src="img/logo.png" class="img-fluid" style="max-height: 40px;" alt="">
        </a>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="navbar-nav mr-auto">
          <li class="nav-item">
              <a class="nav-link waves-effect" href="index.php"><i class="fas fa-user mr-3"></i><?php 
              session_start();
              echo $_SESSION['nome_usuario'];
            ?></a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="index.php"><i class="fas fa-home mr-3"></i>Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="cadastro.php"><i class="fas fa-user mr-3"></i>Cadastrar-se</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="login.php"><i class="fas fa-sign-in-alt mr-3"></i>Entrar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="ver_acoes.php"><i class="fas fa-table mr-3"></i>Atividade</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="ranking.php"><i class="fas fa-code-branch mr-3"></i>Ranking</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="loja.php"><i class="fas fa-shopping-cart mr-3"></i>Loja</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="sobre.php"><i class="fas fa-address-card mr-3"></i>Sobre</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="logout.php"><i class="fas fa-sign-out-alt mr-3"></i>Sair</a>
            </li>
          </ul>

          <!-- Right -->
          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <a href="#" class="nav-link waves-effect">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link waves-effect">
                <i class="fab fa-instagram"></i>
              </a>
            </li>   
            <li class="nav-item">
              <a href="#" class="nav-link waves-effect">
                <i class="fab fa-youtube"></i>
              </a>
            </li>  
          </ul>

        </div>

      </div>
    </nav>
    <!-- Navbar -->

    <!-- Sidebar -->
    <div class="sidebar-fixed position-fixed">

      <a class="logo-wrapper waves-effect">
        <img src="img/logo.png" class="img-fluid" alt="">
      </a>

      <div class="list-group list-group-flush">
        <!-- <a href="#" class="list-group-item waves-effect">
          <i class="fas fa-users-cog mr-3"></i>Administração
        </a> -->
        <a href="index.php" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-user mr-3"></i><?php 
              session_start();
              echo $_SESSION['nome_usuario'];
            ?></a>
        <a href="index.php" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-home mr-3"></i>Início</a>
        <a href="cadastro.php" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-user mr-3"></i>Cadastrar-se</a>
        <a href="login.php" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-sign-in-alt mr-3"></i>Entrar</a>
        <a href="ver_acoes.php" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-table mr-3"></i>Ações</a>
        <a href="ranking.php" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-code-branch mr-3"></i>Ranking</a>
        <a href="loja.php" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-shopping-cart mr-3"></i>Loja</a>
        <a href="sobre.php" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-address-card mr-3"></i>Sobre</a>
          <a href="logout.php" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-sign-out-alt mr-3"></i>Sair</a>
      </div>

    </div>
    <!-- Sidebar -->
    </header>