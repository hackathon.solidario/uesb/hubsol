<?php include "resources/header.php"; ?>

  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
    <!-- Section: Blog v.1 -->
  <section class="my-5">

<!-- Section heading -->
<h2 class="h1-responsive font-weight-bold text-center my-5"><i class="fas fa-hands-helping"></i> Solidarômetro!</h2>
<!-- Section description -->
<p class="text-center w-responsive mx-auto mb-5">
  Aqui você vê como está sua posição no ranking de solidariedade!  
</p>

<!-- Grid row -->
<div class="row">


    <!-- Grid column -->
    <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2">

    <!-- Featured image -->
    <div class="view overlay  mb-lg-0 mb-4">
    <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Avatars/avatar-8.jpg" alt="Sample image">
    <a>
        <div class="mask rgba-white-slight"></div>
    </a>
    </div>

    </div>
    <!-- Grid column -->

    <div class="alert alert-info">
    <!-- Grid column -->
        <div class="col-lg-10 col-md-10 col-xs-10 col-sm-10">

    <!-- Category -->
    <a href="#!" class="green-text">
    <h6 class="font-weight-bold mb-3"><i class="fas fa-medal pr-2"></i>1˚ LUGAR - 1.500 pts</h6>
    </a>
    <!-- Post title -->
    <h3 class="font-weight-bold mb-3"><strong>Nome do usuário</strong></h3>

    </div>
    <!-- Grid column -->
    </div>

    <!-- Grid column -->
    <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2">

        <!-- Featured image -->
        <div class="view overlay  mb-lg-0 mb-4">
        <img class="img-fluid" src="img/oldman.jpg" alt="Sample image">
        <a>
            <div class="mask rgba-white-slight"></div>
        </a>
    </div>

    </div>
    <!-- Grid column -->

    <div class="alert alert-info">
        <!-- Grid column -->
            <div class="col-lg-10 col-md-10 col-xs-10 col-sm-10">

        <!-- Category -->
        <a href="#!" class="green-text">
        <h6 class="font-weight-bold mb-3"><i class="fas fa-medal pr-2"></i>2˚ LUGAR - 1.200 pts</h6>
        </a>
        <!-- Post title -->
        <h3 class="font-weight-bold mb-3"><strong>Nome do usuário</strong></h3>

        </div>
        <!-- Grid column -->
    </div>
    

  <!-- Grid column -->
  <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2">

<!-- Featured image -->
<div class="view overlay  mb-lg-0 mb-4">
<img class="img-fluid" src="img/woman.jpg" alt="Sample image">
<a>
    <div class="mask rgba-white-slight"></div>
</a>
</div>

</div>
<!-- Grid column -->

<div class="alert alert-info">
<!-- Grid column -->
    <div class="col-lg-10 col-md-10 col-xs-10 col-sm-10">

<!-- Category -->
<a href="#!" class="green-text">
<h6 class="font-weight-bold mb-3"><i class="fas fa-medal pr-2"></i>3˚ LUGAR - 1.100 pts</h6>
</a>
<!-- Post title -->
<h3 class="font-weight-bold mb-3"><strong>Nome do usuário</strong></h3>

</div>
<!-- Grid column -->
</div>

<!-- Grid column -->
<div class="col-lg-2 col-md-2 col-xs-2 col-sm-2">

<!-- Featured image -->
<div class="view overlay  mb-lg-0 mb-4">
<img class="img-fluid" src="img/otherwoman.jpg" alt="Sample image">
<a>
    <div class="mask rgba-white-slight"></div>
</a>
</div>

</div>
<!-- Grid column -->

<div class="alert alert-info">
<!-- Grid column -->
    <div class="col-lg-10 col-md-10 col-xs-10 col-sm-10">

<!-- Category -->
<a href="#!" class="green-text">
<h6 class="font-weight-bold mb-3"><i class="fas fa-medal pr-2"></i>4˚ LUGAR - 950 pts</h6>
</a>
<!-- Post title -->
<h3 class="font-weight-bold mb-3"><strong>Nome do usuário</strong></h3>

</div>
<!-- Grid column -->
</div>

</div>
<!-- Grid row -->

<hr class="my-5">

</div>
<!-- Grid row -->

</section>
<!-- Section: Blog v.1 -->
    </div>
  </main>
  <!--Main layout-->

  <?php include "resources/footer.php"; ?>