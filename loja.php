<?php
  include('resources/header.php');
?>
  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
        <h2 class="section-heading">
            Loja de bonificações
        </h2>
        <br><br>
          <div class="row">
        <?php include 'database/BancoConexao.php';
         
            $consulta_loja = $con->query("SELECT produto.imagem,produto.titulo,produto.descricao,produto.pontuacao,usuario.nome
            FROM produto
            INNER JOIN usuario ON usuario.id=produto.fk_usuario
            WHERE produto.status='ATIVO';");
             if($consulta_loja->rowCount()==0){
                 echo "No momento não possui produtos cadastrados em nossa loja solidária.";
             }else{
                while($row_loja = $consulta_loja->fetch(PDO::FETCH_ASSOC)){
            
         ?>
      
          <div class="col-xl-4 col-md-4 col-sm-6 col-xs-12">
            <div class="card">
                <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                <div class="card-body">
                    <h4 class="card-title"><a> <?php echo $row_loja->titulo?></a></h4>
                    <p class="card-text">
                        <h5><b>Descrição:</b></h5>
                       <?php echo $row_loja->descricao?>
                        <br><br>
                        <h5><b>Parceiro:</b> <?php echo $row_loja->nome?></h5>
                        <h5><b>Sol/Pontuação:</b> <?php echo $row_loja->pontuacao?></h5>
                    </p>
                    <?php
                       if($_SESSION['id_cadastro']==""){
                           echo "Cadastre-se para saber quantos sol/pontos esse produto vale.";
                       }else{
                     ?>
                       <button class="btn btn-success btn-block col-12 col-md-2" type="button" onclick='regatar()'>RESGATAR</button>
                    <?php       
                       }
                    ?>
                </div>
            </div>
        </div>
    
<?php  } } ?>
</div>
    </div>

  </main>
  <!--Main layout-->
<?php
  include('resources/footer.php');
?>

</body>

</html>