<?php

function gerarCardsAcoes($titulo, $img, $imgong, $descricao, $link, $data, $vagas){
    echo '<div class="col-lg-4 mb-4">   
  <div class="card promoting-card">
    <div class="card-body d-flex flex-row">
      <img src="'.$imgong.'" class="rounded-circle mr-3" height="50px" width="50px" alt="avatar">
      <div>
        <h4 class="card-title font-weight-bold mb-2">'.$titulo.'</h4>
        <!-- Subtitle -->
        <p class="card-text"><i class="far fa-clock pr-2"></i>'.$data.' | '.$vagas.' vagas</p>

      </div>

    </div>


    <div class="view overlay">
      <img class="card-img-top rounded-0" src="'.$img.'" alt="'.$titulo.'">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>


    <div class="card-body">

      <div class="collapse-content">

        <!-- Text -->
        <p class="card-text collapse" id="collapseContent'.$link.'">'.$descricao.'.</p>
        <!-- Button -->
        <center><a class="btn btn-flat red-text" onclick="abrirAcao(this)" data-toggle="collapse" href="#collapseContent'.$link.'" aria-expanded="false" url="'.$link.'" aria-controls="collapseContent'.$link.'">Veja mais</a></center>

        <i class="fas fa-share-alt text-muted float-right p-1 my-1" data-toggle="tooltip" data-placement="top" title="Share this post"></i>

      </div>

    </div>

  </div>
</div>';
}

?>