<?php
  include('resources/header.php');
  require('resources/componentes/card.php');
  include "database/BancoConexao.php";

  $id = isset($_GET['id']) ? $_GET['id'] : 0;

?>
  <!--Main layout-->
  
      <?php
        $sql = $con->query("SELECT id, titulo, descricao, imagem, quantidade, status FROM acao WHERE status = 'ATIVO' ORDER BY id;");

        while ($acao = $sql->fetch(PDO::FETCH_OBJ)) {
          $titulo = $acao->titulo;
          $imagem = $acao->imagem;
          $descricao = $acao->descricao;
          $id = $acao->id;
          $data_inicial = $acao->data_inicial;
          $quantidade = $acao->quantidade;

        
          echo "

            <main class=\"pt-5 mx-lg-5\">
                <div class=\"container-fluid mt-5 col-12\">
                    <p class=\"h4 mb-4\">$titulo</p>
                    <div class=\"row\">

                        <div class=\"col-xl-12 col-md-12 col-sm-12 col-xs-12\"><img src=\"img/$imagem\" style=\"max-width: 500px;\" /></div>

                        <div class=\"card\"><p>$descricao</p></div>

                    </div>
            
                </div>

            </main>

          ";
        }
      ?>
    

<?php
  include('resources/footer.php');
?>

</body>

</html>