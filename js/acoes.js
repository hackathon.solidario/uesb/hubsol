
//função para abrir a ação após expandir a descrição dela na tela
function abrirAcao(elemento){
	var estado = elemento.getAttribute('aria-expanded');
	var link = elemento.getAttribute('url');
	if(estado=='true'){
		window.location.href = 'acao_detalhe.php?id='+link;
	}else{
		elemento.innerHTML = "Voluntarie-se!";
	}
}

//função para habilitar edição de campos no perfil do usuario
function liberarEditarPerfil(){
	let nome = document.getElementById('nome');
	let telefone = document.getElementById('telefone');
	let cidade = document.getElementById('cidade');
	let estado = document.getElementById('estado');
	let logadouro = document.getElementById('logadouro');

	let btnSalvar = document.getElementById('btnSalvar');
	let btnEditar = document.getElementById('btnEditar');

	let checar = nome.getAttribute('disabled');
	
	if(checar != 'null' || checar != null){
		nome.removeAttribute("disabled");
		telefone.removeAttribute("disabled");
		cidade.removeAttribute("disabled");
		estado.removeAttribute("disabled");
		logadouro.removeAttribute("disabled");
		btnSalvar.removeAttribute("disabled");
		btnEditar.innerHTML = "<i class='fas fa-magic mr-1'></i> CANCELAR EDIÇÃO";
		nome.focus();
	}
	if(checar == 'null' || checar == null){
		nome.setAttribute("disabled", "");
		telefone.setAttribute("disabled", "");
		cidade.setAttribute("disabled", "");
		estado.setAttribute("disabled", "");
		logadouro.setAttribute("disabled", "");
		btnSalvar.setAttribute("disabled", "");
		btnEditar.innerHTML = "<i class='fas fa-magic mr-1'></i> EDITAR PERFIL";
	}
}